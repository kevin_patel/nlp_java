package postagger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;


/*
 * =====================================================================================
 *
 *       Filename:  HMM_Tagger.java
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12 Aug, 2014 11:36:44 PM
 *       Revision:  none
 *       Compiler:  Oracle Java
 *
 *         Author:  Kevin Patel (KP), kevin.patel@cse.iit.ac.in
 *                                    kevin.patel@csa.iisc.ernet.in
 *        Company:  
 *
 * =====================================================================================
 */

public class HMM_Tagger 
{
    public Set< String> states;
    public String most_frequent_tag;

    public HashMap< String, HashMap< String, Double>> trans_prob, emission_prob, suffix_emission_prob;
    
    public HMM_Tagger()
    {
        trans_prob = new HashMap< String, HashMap< String, Double>>();
        emission_prob = new HashMap< String, HashMap< String, Double>>();
        suffix_emission_prob = new HashMap< String, HashMap< String, Double>>();
    }
    
    public ArrayList< String> best_tag_sequence( ArrayList< String> sentence)
    {
        // Data Structures
        HashMap< String, HashMap< Integer, Double>> seq_score = new HashMap< String, HashMap< Integer, Double>>();
        HashMap< String, HashMap< Integer, String>> back_ptr = new HashMap< String, HashMap< Integer, String>>();
        
        // Initialization
        Double max_score, cur_score;
        String candidate;
        int T = sentence.size();
        int N = states.size();
        for( String state: states)
        {
            seq_score.put( state, new HashMap< Integer, Double>());
            back_ptr.put( state, new HashMap< Integer, String>());
            for( Integer i = 0; i < T; i ++)
            {
                seq_score.get( state).put( i, 0.0);
                back_ptr.get( state).put( i, "");
            }
        }
        seq_score.get("SSS").put(0, 1.0);           // BAD CODE ALERT
        back_ptr.get("SSS").put(0, "");             // BAD CODE ALERT
        
        // Iteration
        for( int i = 1; i < T; i++)
        {
            String cur_word = sentence.get( i);
            for( String cur_state: states)
            {
                max_score = Double.MIN_VALUE;
                candidate = "";
                if( emission_prob.containsKey( cur_state) == false)
                    continue;
                if( emission_prob.get( cur_state).containsKey( cur_word) == false)
                    continue;
                Double emit_score = emission_prob.get( cur_state).get( cur_word);
                if( emit_score == 0)
                    continue;
                else
                {
//                    continue;
//                    // max_length 5
//                    int sptr = 1;
//                    if( cur_word.length() - HMM_Trainer.MAX_SUFFIX_LENGTH > 1)
//                        sptr = cur_word.length() - HMM_Trainer.MAX_SUFFIX_LENGTH;
//
//                    for( int index = sptr; index < cur_word.length(); index ++)
//                    {
//                        String cur_suffix = cur_word.substring( index);
//                        if( suffix_emission_prob.get( cur_state).containsKey( cur_suffix) == true)
//                        {
//                            emit_score = suffix_emission_prob.get( cur_state).get( cur_suffix);
//                            if( emit_score > 0)
//                                break;
//                        }
//                    }
                }
                for( String prev_state: states)
                {
                    if( prev_state.equals("."))
                        continue;
                    Double prev_score = seq_score.get( prev_state).get( i - 1);
                    if( trans_prob.containsKey( prev_state) == false)
                        continue;
                    if( trans_prob.get( prev_state).containsKey( cur_state) == false)
                        continue;
                    Double trans_score = trans_prob.get( prev_state).get( cur_state);

                    cur_score = prev_score * trans_score * emit_score;
                    if( max_score < cur_score)
                    {
                        max_score = cur_score;
                        candidate = prev_state;
                    }
                }
                if( candidate.equals( ""))
                    candidate = most_frequent_tag;
                seq_score.get( cur_state).put(i, max_score);
                back_ptr.get( cur_state).put(i, candidate);
            }
        }
        
        ArrayList< String> C = new ArrayList< String>();
        for( int i = 0; i < T; i++)
            C.add( "");
        max_score = Double.MIN_VALUE;
        candidate = "";
        for( String state: states)
        {
            cur_score = seq_score.get( state).get( T - 1);
            if( max_score < cur_score)
            {
                max_score = cur_score;
                candidate = state;
            }
        }
        
        // something wicked
        if( candidate.equals(""))
        {
            C.set( 0, "SSS");
            for( int i = 1; i < C.size() - 1; i++)
                C.set( i, most_frequent_tag);
            C.set( T - 1, ".");
        }
        else
        {
            C.set( T - 1, candidate);
            for( int i = T - 2; i >= 0; i--)
                C.set( i, back_ptr.get( C.get(i + 1)).get(i + 1));            
        }

        return C;
    }
    
    
    public Set<String> getStates() 
    {
        return states;
    }

    public void setStates(Set<String> states) 
    {
        this.states = states;
    }

    public HashMap<String, HashMap<String, Double>> getTrans_prob() 
    {
        return trans_prob;
    }

    public void setTrans_prob(HashMap<String, HashMap<String, Double>> trans_prob) 
    {
        this.trans_prob = trans_prob;
    }

    public HashMap<String, HashMap<String, Double>> getEmission_prob() 
    {
        return emission_prob;
    }

    public void setEmission_prob(HashMap<String, HashMap<String, Double>> emission_prob) 
    {
        this.emission_prob = emission_prob;
    }

    public HashMap<String, HashMap<String, Double>> getSuffix_emission_prob() 
    {
        return suffix_emission_prob;
    }

    public void setSuffix_emission_prob(HashMap<String, HashMap<String, Double>> suffix_emission_prob) 
    {
        this.suffix_emission_prob = suffix_emission_prob;
    }
    
    public String getMost_frequent_tag() 
    {
        return most_frequent_tag;
    }

    public void setMost_frequent_tag(String most_frequent_tag) 
    {
        this.most_frequent_tag = most_frequent_tag;
    }
}
