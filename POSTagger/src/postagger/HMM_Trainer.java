package postagger;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/*
 * =====================================================================================
 *
 *       Filename:  HMM_Trainer.java
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12 Aug, 2014 11:36:33 PM
 *       Revision:  none
 *       Compiler:  Oracle Java
 *
 *         Author:  Kevin Patel (KP), kevin.patel@cse.iit.ac.in
 *                                    kevin.patel@csa.iisc.ernet.in
 *        Company:  
 *
 * =====================================================================================
 */

public class HMM_Trainer 
{
    public Set< String> tagset, vocab, suffix_vocab;
    public HashMap< String, Integer> unigram_freq;
    public HashMap< String, HashMap< String, Integer>> bigram_freq;
    public HashMap< String, HashMap< String, Integer>> emission_freq;
    public HashMap< String, HashMap< String, Integer>> suffix_emission_freq;
    public static Integer MAX_SUFFIX_LENGTH = 5;
    public Integer token_cnt = 0, bigram_cnt = 0;
    public Double lambda1 = 0.31, lambda2 = 0.69;
    
    public HMM_Trainer()
    {
        vocab = new HashSet< String>();
        tagset = new HashSet< String>();
        suffix_vocab = new HashSet< String>();
        unigram_freq = new HashMap< String, Integer>();
        bigram_freq = new HashMap< String, HashMap< String, Integer>>();
        emission_freq = new HashMap< String, HashMap< String, Integer>>();
        suffix_emission_freq = new HashMap< String, HashMap< String, Integer>>();
    }
    
    public HMM_Trainer( HashSet< String> vocab, HashSet< String> tagset, HashSet< String> suffix_vocab)
    {
        this.vocab = vocab;
        this.tagset = tagset;
        this.suffix_vocab = suffix_vocab;
        
        // Adding unknown Tag UNK
        //this.tagset.add( "UNK");
        
        // Init freq matrices
        unigram_freq = new HashMap< String, Integer>();
        bigram_freq = new HashMap< String, HashMap< String, Integer>>();
        emission_freq = new HashMap< String, HashMap< String, Integer>>();
        suffix_emission_freq = new HashMap< String, HashMap< String, Integer>>();        
        
//        System.out.println( "Setting up");
//        for( String tag: tagset)
//        {
//            unigram_freq.put( tag, 0);
//            bigram_freq.put( tag, new HashMap< String, Integer>());
//            emission_freq.put( tag, new HashMap< String, Integer>());
//            suffix_emission_freq.put( tag, new HashMap< String, Integer>());
//        }
//        
//        System.out.println( "Initializing bigrams matrix " + tagset.size() + " x " + tagset.size());
//        for( String ptag: tagset)
//            for( String ctag: tagset)
//                bigram_freq.get( ptag).put( ctag, 0);
//        
//        System.out.println( "Initializing emission matrix " + tagset.size() + " x " + vocab.size());
//        for( String tag: tagset)
//            for( String word: vocab)
//                emission_freq.get( tag).put( word, 0);
//        
//        System.out.println( "Initializing suffix emission matrix "  + tagset.size() + " x " + suffix_vocab.size());
//        for( String tag: tagset)
//            for( String suffix: suffix_vocab)
//                suffix_emission_freq.get( tag).put( suffix, 0);
    }
    
    public HMM_Fast_Tagger train_supervised( ArrayList< ArrayList< String>> train_data, ArrayList< ArrayList< String>> train_labels, String mft)
    {
        // Calculating raw frequencies
        System.out.println( "Training");
        Integer tmp = 0;
        for( int rptr = 0; rptr < train_data.size(); rptr ++)
        {
            System.out.println( "Training on sentence no : " + rptr);
            ArrayList< String> cur_data = train_data.get( rptr);
            ArrayList< String> cur_labels = train_labels.get( rptr);
            
            String start_word = cur_data.get( 0);
            String start_tag = cur_labels.get( 0);
            
            update_unigram( start_tag);
            update_emission( start_tag, start_word);
            
            String prev_tag = start_tag;
            token_cnt ++;
            
            for( int cptr = 1; cptr < cur_data.size(); cptr ++)
            {
                String cur_word = cur_data.get( cptr);
                String cur_tag = cur_labels.get( cptr);
                update_unigram( cur_tag);
                update_bigram( prev_tag, cur_tag);
                update_emission( cur_tag, cur_word);
                prev_tag = cur_tag;
                token_cnt ++;
                bigram_cnt ++;
            }
        }
        
        HMM_Fast_Tagger hmm_fast_tagger = new HMM_Fast_Tagger( tagset, mft, token_cnt, tagset.size(), bigram_cnt, unigram_freq, bigram_freq, emission_freq);
        return hmm_fast_tagger;
        // Normalize and generate Tagger instance
//        System.out.println( "Normalizing");
//        HMM_Tagger hmm_tagger = new HMM_Tagger();
//        hmm_tagger.setStates( tagset);
//        hmm_tagger.setTrans_prob( get_smooth_bigram_trans( get_bigram_trans()));
//        hmm_tagger.setEmission_prob( get_raw_emission());
//        hmm_tagger.setSuffix_emission_prob( get_raw_suffix_emission());
//        hmm_tagger.setMost_frequent_tag( mft);
//        return hmm_tagger;
    }
    
    public HashMap< String, HashMap< String, Double>> get_bigram_trans()
    {
        HashMap< String, HashMap< String, Double>> ret = new HashMap< String, HashMap< String, Double>>();
        for( String prev_tag: unigram_freq.keySet())
        {
            HashMap< String, Double> tmp_mp = new HashMap< String, Double>();
            Double den = unigram_freq.get( prev_tag) * 1.0;
            for( String cur_tag: bigram_freq.get(prev_tag).keySet())
            {
                Double num = bigram_freq.get( prev_tag).get( cur_tag) * 1.0;
                if( num == 0 && den == 0)
                    tmp_mp.put( cur_tag, 0.0);
                else
                    tmp_mp.put( cur_tag, num / den);
            }
            ret.put( prev_tag, tmp_mp);
        }
        return ret;
    }
    
    public HashMap< String, HashMap< String, Double>> get_smooth_bigram_trans( HashMap< String, HashMap< String, Double>> raw_mp)
    {
        HashMap< String, HashMap< String, Double>> ret = new HashMap< String, HashMap< String, Double>>();
        for( String prev_tag: unigram_freq.keySet())
        {
            HashMap< String, Double> tmp_mp = new HashMap< String, Double>();
            ret.put( prev_tag, tmp_mp);
            for( String cur_tag: bigram_freq.get( prev_tag).keySet())
            {
                Double op1 = ( lambda1 * unigram_freq.get( cur_tag) / token_cnt);
                Double op2 = ( lambda2 * raw_mp.get( prev_tag).get( cur_tag));
                Double op3 = op1 * op2;
                ret.get( prev_tag).put( cur_tag, op3);
            }
        }
        return ret;
    }
    
    public HashMap< String, HashMap< String, Double>> get_raw_emission()
    {
        HashMap< String, HashMap< String, Double>> ret = new HashMap< String, HashMap< String, Double>>();
        for( String cur_tag: unigram_freq.keySet())
        {
            HashMap< String, Double> tmp_mp = new HashMap< String, Double>();
            Double den = unigram_freq.get( cur_tag) * 1.0;
            for( String cur_word: emission_freq.get( cur_tag).keySet())
            {
                Double num = emission_freq.get( cur_tag).get( cur_word) * 1.0;
                if( num == 0 && den == 0)
                    tmp_mp.put( cur_word, 0.0);
                else
                {
                    tmp_mp.put( cur_word, num / den);
                }
            }
            ret.put( cur_tag, tmp_mp);
        }
        return ret;
    }
    
    public HashMap< String, HashMap< String, Double>> get_raw_suffix_emission()
    {
        HashMap< String, HashMap< String, Double>> ret = new HashMap< String, HashMap< String, Double>>();
//        for( String cur_tag: unigram_freq.keySet())
//        {
//            HashMap< String, Double> tmp_mp = new HashMap< String, Double>();
//            Double den = unigram_freq.get( cur_tag) * 1.0;
//            Double num = 0.0;
//            for( String cur_suffix: suffix_emission_freq.get( cur_tag).keySet())
//            {
//                try
//                {
//                    num = suffix_emission_freq.get( cur_tag).get( cur_suffix) * 1.0;
//                }
//                catch( NullPointerException e)
//                {
//                    System.out.println( cur_tag + " " + cur_suffix);
//                }
//                if( num == 0 && den == 0)
//                    tmp_mp.put( cur_suffix, 0.0);
//                else
//                {
//                    if( cur_tag.equals( "UNK"))
//                        tmp_mp.put( cur_suffix, (num / den) * unigram_freq.get( "UNK"));
//                    else
//                        tmp_mp.put( cur_suffix, num / den);
//                    
//                }
//            }
//            ret.put( cur_tag, tmp_mp);
//        }
        return ret;
    }
    
    private int C( HashMap< String, Integer> map, String key)
    {
        if( map.containsKey( key))
            return map.get( key);
        return 0;
    }
    
    private int C( HashMap< String, HashMap< String, Integer>> map, String key1, String key2)
    {
        if( map.containsKey( key1))
            if( map.get( key1).containsKey( key2))
                return map.get( key1).get( key2);
        return 0;
    }
    
    private void update_unigram( String key)
    {
        if( unigram_freq.containsKey( key) == false)
            unigram_freq.put( key, 0);
        Integer val = unigram_freq.get( key);
        unigram_freq.put( key, val + 1);
    }
    
    private void update_bigram( String key1, String key2)
    {
        if( bigram_freq.containsKey( key1) == false)
            bigram_freq.put( key1, new HashMap< String, Integer>());
        if( bigram_freq.get( key1).containsKey( key2) == false)
            bigram_freq.get( key1).put( key2, 0);
        Integer val = bigram_freq.get( key1).get( key2);
        bigram_freq.get(key1).put( key2, val + 1);
    }
    
    private void update_emission( String key1, String key2)
    {
        if( emission_freq.containsKey( key1) == false)
            emission_freq.put( key1, new HashMap< String, Integer>());
        if( emission_freq.get( key1).containsKey( key2) == false)
            emission_freq.get( key1).put( key2, 0);
        Integer val = emission_freq.get( key1).get( key2);
        emission_freq.get(key1).put( key2, val + 1);
    }
    
}
