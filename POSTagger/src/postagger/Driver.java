package postagger;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;


/*
 * =====================================================================================
 *
 *       Filename:  Driver.java
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12 Aug, 2014
 *       Revision:  none
 *       Compiler:  Oracle Java
 *
 *         Author:  Kevin Patel (KP), kevin.patel@cse.iitb.ac.in
 *                                    kevin.patel@csa.iisc.ernet.in
 *        Company:  
 *
 * =====================================================================================
 */
public class Driver 
{

    public boolean get_statistics = true;
    public String corpus_fname = "data/proc_brown.txt";
    public ArrayList< ArrayList< String>> all_data, train_data, test_data;
    public ArrayList< ArrayList< String>> all_labels, train_labels, test_labels;
    
    public String vocab_fname = "data/original_vocab.bin";
    public String tagset_fname = "data/original_tagset.bin";
    public String suffix_vocab_fname = "data/original_suffix_vocab.bin";
    public String word_freq_fname = "data/original_word_freq.bin";
    public String tag_freq_fname = "data/original_tag_freq.bin";
    public String suffix_freq_fname = "data/original_suffix_freq.bin";
    
    public int[] start, end;
    
    public Driver()
    {
        all_data = new ArrayList< ArrayList< String>>();
        train_data = new ArrayList< ArrayList< String>>();
        test_data = new ArrayList< ArrayList< String>>();
        all_labels = new ArrayList< ArrayList< String>>();
        train_labels = new ArrayList< ArrayList< String>>();
        test_labels = new ArrayList< ArrayList< String>>();
        start = new int[]{ 0, 10688, 21376, 32064, 42752};
        end = new int[]{ 10688, 21376, 32064, 42752, 53440};
    }
    
    public static void main(String[] args) 
            throws IOException
    {
        Driver driver = new Driver();
        
        // reading corpus
        BufferedReader corpus_br = new BufferedReader( new FileReader( driver.corpus_fname));
        String cur_tagged_line = "";
        while( true)
        {
            cur_tagged_line = corpus_br.readLine();
            if( cur_tagged_line == null)
                break;            
            ArrayList< ArrayList< String>> pair_data = Driver.parse_tagged_line(cur_tagged_line);
            driver.all_data.add( pair_data.get( 0));
            driver.all_labels.add( pair_data.get( 1));
        }
        // Done
        
        // preprocessing and generating statistics
        Preprocessor preproc = new Preprocessor();
        // generating and serializing vocab
        HashSet< String> vocab = preproc.get_vocab( driver.all_data);
        HashSet< String> tagset = preproc.get_tagset( driver.all_labels);
        HashSet< String> suffix_vocab = preproc.get_suffix_vocab( driver.all_data);
//        try
//        {
//            FileOutputStream fout = new FileOutputStream( driver.suffix_vocab_fname);
//            ObjectOutputStream oout = new ObjectOutputStream( fout);
//            oout.writeObject( suffix_vocab);
//            oout.close();
//            fout.close();
//        }
//        catch( Exception e)
//        {
//        }       
//        // Done
        
        // testing HMM trainer
        System.out.println( "Creating HMM trainer");
        HMM_Trainer hmm_trainer = new HMM_Trainer( vocab, tagset, suffix_vocab);
        System.out.println( "Training");
        HMM_Fast_Tagger hmm_fast_tagger = hmm_trainer.train_supervised( driver.all_data, driver.all_labels, preproc.get_most_freq_tag());
        hmm_fast_tagger.prepare_gt_counts();
        
        BufferedReader inp_br = new BufferedReader( new InputStreamReader( System.in));
        String test_line = "";
        while( true)
        {
            test_line = inp_br.readLine();
            ArrayList< String> input_sentence = new ArrayList< String>( Arrays.asList( test_line.split(" ")));
            ArrayList< String> pred_labels = hmm_fast_tagger.best_tag_sequence( input_sentence);
            System.out.println( pred_labels);
        }
    }
    
    public static ArrayList< ArrayList< String>> parse_tagged_line( String tagged_sentence)
    {
        ArrayList< ArrayList< String>> ret = new ArrayList< ArrayList< String>>();
        ArrayList< String> words, tags;
        words = new ArrayList<String>();
        tags = new ArrayList<String>();        
        String[] tokens = tagged_sentence.split(" ");
        for( int i = 0; i < tokens.length; i++)
        {
            String[] word_tag = tokens[ i].split("_");
            words.add(word_tag[ 0]);
            tags.add(word_tag[ 1]);
        }
        ret.add( words);
        ret.add( tags);
        return ret;
    }
    
    
}
