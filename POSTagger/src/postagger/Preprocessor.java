package postagger;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
 * =====================================================================================
 *
 *       Filename:  Preprocessor.java
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12 Aug, 2014 1:51:39 PM
 *       Revision:  none
 *       Compiler:  Oracle Java
 *
 *         Author:  Kevin Patel (KP), kevin.patel@cse.iit.ac.in
 *                                    kevin.patel@csa.iisc.ernet.in
 *        Company:  
 *
 * =====================================================================================
 */

public class Preprocessor 
{

    public HashMap< String, Integer> word_freq, tag_freq, suffix_freq;
    
    public Preprocessor()
    {
        word_freq = new HashMap< String, Integer>();
        tag_freq = new HashMap< String, Integer>();
        suffix_freq = new HashMap< String, Integer>();
    }
    
    public HashSet< String> get_vocab( ArrayList< ArrayList< String>> data)
    {
        HashSet< String> vocab = new HashSet< String>();
        for( int rptr = 0; rptr < data.size(); rptr ++)
            for( int cptr = 0; cptr < data.get( rptr).size(); cptr ++) 
            {
                String cur_word = data.get( rptr).get( cptr);
                //cur_word = cur_word.toLowerCase();
                vocab.add( cur_word);
                if( word_freq.containsKey( cur_word) == false)
                    word_freq.put( cur_word, 0);
                Integer freq = word_freq.get( cur_word);
                word_freq.put( cur_word, freq + 1);
            }
        return vocab;
    }
    
    public HashSet< String> get_tagset( ArrayList< ArrayList< String>> labels)
    {
        HashSet< String> tagset = new HashSet< String>();
        for( int rptr = 0; rptr < labels.size(); rptr ++)
            for( int cptr = 0; cptr < labels.get( rptr).size(); cptr ++) 
            {
                String cur_tag = labels.get( rptr).get( cptr);
                tagset.add( cur_tag);
                if( tag_freq.containsKey( cur_tag) == false)
                    tag_freq.put( cur_tag, 0);
                Integer freq = tag_freq.get( cur_tag);
                tag_freq.put( cur_tag, freq + 1);
            }
        return tagset;
    }    
    
    public HashSet< String> get_unknown_words( ArrayList< ArrayList< String>> data, HashSet< String> vocab)
    {
        HashSet< String> retset = new HashSet< String>();
        for( int rptr = 0; rptr < data.size(); rptr ++)
            for( int cptr = 0; cptr < data.get( rptr).size(); cptr ++)
            {
                String cur_word = data.get( rptr).get( cptr);
                if( vocab.contains( cur_word) == false)
                    retset.add( cur_word);
            }
        return retset;
    }
    
    public HashSet< String> get_unknown_tags( ArrayList< ArrayList< String>> labels, HashSet< String> tagset)
    {
        HashSet< String> retset = new HashSet< String>();
        for( int rptr = 0; rptr < labels.size(); rptr ++)
            for( int cptr = 0; cptr < labels.get( rptr).size(); cptr ++)
            {
                String cur_tag = labels.get( rptr).get( cptr);
                if( tagset.contains( cur_tag) == false)
                    retset.add( cur_tag);
            }
        return retset;
    }    
    
    public HashSet< String> get_suffix_vocab( ArrayList< ArrayList< String>> data)
    {
        HashSet< String> suffix_vocab = new HashSet< String>();
        for( int rptr = 0; rptr < data.size(); rptr ++)
            for( int cptr = 0; cptr < data.get( rptr).size(); cptr ++) 
            {
                String cur_word = data.get( rptr).get( cptr);
                int sptr = 1;
                if( cur_word.length() - HMM_Trainer.MAX_SUFFIX_LENGTH > 1)
                    sptr = cur_word.length() - HMM_Trainer.MAX_SUFFIX_LENGTH;
                for( int wptr = sptr; wptr < cur_word.length(); wptr ++)
                {
                    String cur_suffix = cur_word.substring( wptr);
                    suffix_vocab.add( cur_suffix);
                    if( suffix_freq.containsKey( cur_suffix) == false)
                        suffix_freq.put( cur_suffix, 0);
                    Integer freq = suffix_freq.get( cur_suffix);
                    suffix_freq.put( cur_suffix, freq + 1);
                }
            }
        return suffix_vocab;
    }

        
    public HashMap< String, Integer> get_word_freq()
    {
        return word_freq;
    }
    
    public HashMap< String, Integer> get_tag_freq()
    {
        return tag_freq;
    }
    
    public HashMap< String, Integer> get_suffix_freq()
    {
        return suffix_freq;
    }
    
    public void most_freq_word( int k)
    {
        Map< String, Integer> sorted_word_freq = sortByComparatorDesc( word_freq);
        Iterator it = sorted_word_freq.entrySet().iterator();
        int cnt = 0;
        while( it.hasNext())
        {
            Map.Entry cur_pair = (Map.Entry)it.next();
            System.out.println( cur_pair.getKey() + " " + cur_pair.getValue());
            cnt ++;
            if( cnt >= k)
                break;
        }
    }
    
    public void get_most_freq_word( int k)
    {
        Map< String, Integer> sorted_word_freq = sortByComparatorDesc( word_freq);
        Iterator it = sorted_word_freq.entrySet().iterator();
        int cnt = 0;
        while( it.hasNext())
        {
            Map.Entry cur_pair = (Map.Entry)it.next();
            System.out.println( cur_pair.getKey() + " " + cur_pair.getValue());
            cnt ++;
            if( cnt >= k)
                break;
        }
    }
    
    public void most_freq_tags( int k)
    {
        Map< String, Integer> sorted_tag_freq = sortByComparatorDesc( tag_freq);
        Iterator it = sorted_tag_freq.entrySet().iterator();
        int cnt = 0;
        while( it.hasNext())
        {
            Map.Entry cur_pair = (Map.Entry)it.next();
            System.out.println( cur_pair.getKey() + " " + cur_pair.getValue());
            cnt ++;
            if( cnt >= k)
                break;
        }
    }
    
    public String get_most_freq_tag()
    {
        Map< String, Integer> sorted_tag_freq = sortByComparatorDesc( tag_freq);
        Iterator it = sorted_tag_freq.entrySet().iterator();
        int cnt = 0;
        while( it.hasNext())
        {
            Map.Entry cur_pair = (Map.Entry)it.next();
            return cur_pair.getKey().toString();
//            System.out.println( cur_pair.getKey() + " " + cur_pair.getValue());
//            cnt ++;
//            if( cnt >= k)
//                break;
        }
        return null;
    }
    
    public void most_freq_suffixes( int k)
    {
        Map< String, Integer> sorted_suffix_freq = sortByComparatorDesc( suffix_freq);
        Iterator it = sorted_suffix_freq.entrySet().iterator();
        int cnt = 0;
        while( it.hasNext())
        {
            Map.Entry cur_pair = (Map.Entry)it.next();
            System.out.println( cur_pair.getKey() + " " + cur_pair.getValue());
            cnt ++;
            if( cnt >= k)
                break;
        }
    }
    
    public void least_freq_word( int k)
    {
        Map< String, Integer> sorted_word_freq = sortByComparator( word_freq);
        Iterator it = sorted_word_freq.entrySet().iterator();
        int cnt = 0;
        while( it.hasNext())
        {
            Map.Entry cur_pair = (Map.Entry)it.next();
            System.out.println( cur_pair.getKey() + " " + cur_pair.getValue());
            cnt ++;
            if( cnt >= k)
                break;
        }
    }
    
    public void least_freq_tags( int k)
    {
        Map< String, Integer> sorted_tag_freq = sortByComparator( tag_freq);
        Iterator it = sorted_tag_freq.entrySet().iterator();
        int cnt = 0;
        while( it.hasNext())
        {
            Map.Entry cur_pair = (Map.Entry)it.next();
            System.out.println( cur_pair.getKey() + " " + cur_pair.getValue());
            cnt ++;
            if( cnt >= k)
                break;
        }
    }
    
    public void least_freq_suffixes( int k)
    {
        Map< String, Integer> sorted_suffix_freq = sortByComparator( suffix_freq);
        Iterator it = sorted_suffix_freq.entrySet().iterator();
        int cnt = 0;
        while( it.hasNext())
        {
            Map.Entry cur_pair = (Map.Entry)it.next();
            System.out.println( cur_pair.getKey() + " " + cur_pair.getValue());
            cnt ++;
            if( cnt >= k)
                break;
        }
    }
    
    public void tags_with_cnt_one()
    {
        Map< String, Integer> sorted_tag_freq = sortByComparator( tag_freq);
        Iterator it = sorted_tag_freq.entrySet().iterator();
        int cnt = 0;
        while( it.hasNext())
        {
            Map.Entry cur_pair = (Map.Entry)it.next();
            if( (Integer)cur_pair.getValue() == 1)
            {
                System.out.println( cur_pair.getKey() + " " + cur_pair.getValue());
                cnt ++;
            }
            else
                break;
        }
        System.out.println( "Tags with count one = " + cnt);
    }
    
    // following code snippets taken from http://www.mkyong.com/java/how-to-sort-a-map-in-java/
    private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap) 
    {
        // Convert Map to List
        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());
        
        // Sort list with comparator, to compare the Map values
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() 
        {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) 
            {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        
        // Convert sorted map back to a Map
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) 
        {
            Map.Entry<String, Integer> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }
    
    private static Map<String, Integer> sortByComparatorDesc(Map<String, Integer> unsortMap) 
    {
        // Convert Map to List
        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());
        
        // Sort list with comparator, to compare the Map values
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() 
        {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) 
            {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });
        
        // Convert sorted map back to a Map
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) 
        {
            Map.Entry<String, Integer> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }
}
