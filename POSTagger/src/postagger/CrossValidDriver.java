/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package postagger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author coed-forensic
 */
public class CrossValidDriver 
{

    public boolean get_statistics = true;
    public String corpus_fname = "data/proc_brown.txt";
    public ArrayList< ArrayList< String>> all_data, train_data, test_data;
    public ArrayList< ArrayList< String>> all_labels, train_labels, test_labels;
    
    public String vocab_fname = "data/original_vocab.bin";
    public String tagset_fname = "data/original_tagset.bin";
    public String suffix_vocab_fname = "data/original_suffix_vocab.bin";
    public String word_freq_fname = "data/original_word_freq.bin";
    public String tag_freq_fname = "data/original_tag_freq.bin";
    public String suffix_freq_fname = "data/original_suffix_freq.bin";
    
    public int[] start, end;
    
    public CrossValidDriver()
    {
        all_data = new ArrayList< ArrayList< String>>();
        train_data = new ArrayList< ArrayList< String>>();
        test_data = new ArrayList< ArrayList< String>>();
        all_labels = new ArrayList< ArrayList< String>>();
        train_labels = new ArrayList< ArrayList< String>>();
        test_labels = new ArrayList< ArrayList< String>>();
        start = new int[]{ 0, 10688, 21376, 32064, 42752};
        //end = new int[]{ 100, 21376, 32064, 42752, 53440};
        end = new int[]{ 10688, 21376, 32064, 42752, 53440};
    }
    
    public static void main(String[] args) 
            throws IOException
    {
        CrossValidDriver driver = new CrossValidDriver();
        
        // reading corpus
        BufferedReader corpus_br = new BufferedReader( new FileReader( driver.corpus_fname));
        String cur_tagged_line = "";
        while( true)
        {
            cur_tagged_line = corpus_br.readLine();
            if( cur_tagged_line == null)
                break;            
            ArrayList< ArrayList< String>> pair_data = Driver.parse_tagged_line(cur_tagged_line);
            driver.all_data.add( pair_data.get( 0));
            driver.all_labels.add( pair_data.get( 1));
        }
        Preprocessor preproc = new Preprocessor();
        HashSet< String> tagset = preproc.get_tagset( driver.all_labels);
        // Done
        
        Double P = 0.0, R = 0.0, F = 0.0, A = 0.0;
        for( int fptr = 0; fptr < 5; fptr ++)
        {
            System.out.println( "Running fold " + fptr);
            // preparing current fold
            driver.prepare_train( fptr);
            driver.prepare_test( fptr);
            
            //driver.test_data = driver.train_data;
            //driver.test_labels = driver.train_labels;
            
            // preprocessing and generating statistics
            // generating and serializing vocab
            HashSet< String> vocab = preproc.get_vocab( driver.train_data);
            //HashSet< String> tagset = preproc.get_tagset( driver.train_labels);
            HashSet< String> suffix_vocab = preproc.get_suffix_vocab( driver.train_data);
            // Done

            // training HMM tagger
            System.out.println( "Creating HMM trainer");
            HMM_Trainer hmm_trainer = new HMM_Trainer( vocab, tagset, suffix_vocab);
            System.out.println( "Training");
            String mft = preproc.get_most_freq_tag();
            HMM_Fast_Tagger hmm_fast_tagger = hmm_trainer.train_supervised( driver.train_data, driver.train_labels, mft);
            hmm_fast_tagger.prepare_gt_counts();
            //HMM_Tagger hmm_tagger = hmm_trainer.train_supervised( driver.train_data, driver.train_labels, mft);
            
            // testing
            Double prec = 0.0, rec = 0.0, fmeasure, acc = 0.0; 
            for( int rptr = 0; rptr < driver.test_data.size(); rptr ++)
            {
                System.out.println( "Testing sentence " + rptr);
                ArrayList< String> raw_sentence = driver.test_data.get( rptr);
                ArrayList< String> true_labels = driver.test_labels.get( rptr);
                ArrayList< String> pred_labels = hmm_fast_tagger.best_tag_sequence( raw_sentence);
                HashMap< String, HashMap< String, Integer>> conf_mat = driver.calculate_confusion_matrix( true_labels, pred_labels);
                prec += driver.calculate_precision( conf_mat);
                rec += driver.calculate_recall( conf_mat);
                acc += driver.calculate_accuracy( true_labels, pred_labels);
            }
            prec /= driver.test_data.size();
            rec /= driver.test_data.size();
            acc /= driver.test_data.size();
            fmeasure = ( 2 * prec * rec) / ( prec + rec);
            System.out.println( "Fold " + fptr + "\t" + prec + "\t" + rec + "\t" + fmeasure);
            P += prec;
            R += rec;
            A += acc;
        }
        P /= 5;
        R /= 5;
        A /= 5;
        F = ( 2 * P * R) / ( P + R);
        System.out.println( "Overall: \t" + P + "\t" + R + "\t" + F + "\t" + A + "\t" + HMM_Fast_Tagger.error_cnt);;
                
    }
    
    public static ArrayList< ArrayList< String>> parse_tagged_line( String tagged_sentence)
    {
        ArrayList< ArrayList< String>> ret = new ArrayList< ArrayList< String>>();
        ArrayList< String> words, tags;
        words = new ArrayList<String>();
        tags = new ArrayList<String>();        
        String[] tokens = tagged_sentence.split(" ");
        for( int i = 0; i < tokens.length; i++)
        {
            String[] word_tag = tokens[ i].split("_");
            String lower_word = word_tag[ 0].toLowerCase();
            lower_word = word_tag[ 0].toLowerCase();
            lower_word = lower_word.toLowerCase();
            words.add( lower_word);
            tags.add(word_tag[ 1]);
        }
        ret.add( words);
        ret.add( tags);
        return ret;
    }
    
    private void prepare_train( int fold)
    {
        train_data.clear();
        train_labels.clear();
        for( int i = 0; i < 5; i ++)
        {
            if( i == fold)
                continue;
            for( int ptr = start[ i]; ptr < end[ i]; ptr ++)
            {
                train_data.add( all_data.get( ptr));
                train_labels.add( all_labels.get( ptr));
            }
            
        }
    }
    
    private void prepare_test( int fold)
    {
        test_data.clear();
        test_labels.clear();
        for( int ptr = start[ fold]; ptr < end[ fold]; ptr ++)
        {
            test_data.add( all_data.get( ptr));
            test_labels.add( all_labels.get( ptr));
        }
    }
    
    public HashMap< String, HashMap< String, Integer>> calculate_confusion_matrix( ArrayList< String> true_labels, ArrayList< String> pred_labels)
    {
        HashMap< String, HashMap< String, Integer>> retmap = new HashMap< String, HashMap< String, Integer>>();
        HashSet< String> local_tagset = new HashSet< String>();
        for( String tag: true_labels)
            local_tagset.add( tag);
        for( String tag: pred_labels)
            local_tagset.add( tag);
        for( String tag: local_tagset)
            retmap.put( tag, new HashMap< String, Integer>());
        //retmap.put( "UNK", new HashMap< String, Integer>());
        for( String rtag: local_tagset)
            for( String ctag: local_tagset)
                retmap.get( rtag).put( ctag, 0);
        //retmap.get( "UNK").put( "UNK", 0);
        for( int cptr = 0; cptr < true_labels.size(); cptr ++)
        {
            String rtag = true_labels.get( cptr);
            String ctag = pred_labels.get( cptr);
            Integer val = retmap.get( rtag).get( ctag);
            try
            {
                retmap.get( rtag).put( ctag, val + 1);
            }
            catch( Exception e)
            {
                //System.out.println( rtag + " " + ctag);
                throw e;
            }
        }
        return retmap;
    }
    
    public Double calculate_accuracy( ArrayList< String> true_labels, ArrayList< String> pred_labels)
    {
        Double ans = true_labels.size() * 1.0;
        for( int i = 0; i < true_labels.size(); i ++)
            if( true_labels.get( i).equals( pred_labels.get( i)) == false)
                ans --;
        ans /= true_labels.size();
        return ans;
    }
    
    public Double calculate_precision( HashMap< String, HashMap< String, Integer>> confusion_matrix)
    {
        Double total = 0.0;
        int cnt = 0;
        for( String ctag: confusion_matrix.keySet())
        {
            Double val = 0.0;
            for( String rtag: confusion_matrix.keySet())
                val += confusion_matrix.get( rtag).get( ctag);
            if( confusion_matrix.get( ctag).get( ctag) == 0 && val == 0)
                continue;
            total += confusion_matrix.get( ctag).get( ctag) / val;
            cnt ++;
        }
        total /= cnt;
        return total;
    }
    
    public Double calculate_recall( HashMap< String, HashMap< String, Integer>> confusion_matrix)
    {
        Double total = 0.0;
        int cnt = 0;
        for( String rtag: confusion_matrix.keySet())
        {
            Double val = 0.0;
            for( String ctag: confusion_matrix.keySet())
                val += confusion_matrix.get( rtag).get( ctag);
            if( confusion_matrix.get( rtag).get( rtag) == 0 && val == 0)
                continue;
            total += confusion_matrix.get( rtag).get( rtag) / val;
            cnt ++;
        }
        total /= cnt;
        return total;
    }
}
