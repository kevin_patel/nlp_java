/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package postagger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author coed-forensic
 */
public class HMM_Fast_Tagger 
{

    public Set< String> states;
    public String most_frequent_tag;
    public int token_cnt;
    public int tag_cnt;
    public int bigram_cnt;
    public HashMap< String, Integer> unigram_freq;
    public HashMap< String, HashMap< String, Integer>> bigram_freq;
    public HashMap< String, HashMap< String, Integer>> emission_freq;
    
    public HashMap< String, Double> gt_unigram_freq;
    public HashMap< String, HashMap< String, Double>> gt_bigram_freq;
    public HashMap< String, HashMap< String, Double>> gt_emission_freq;
    
    public HashMap< Integer, Integer> bigram_with_count_freq, emission_with_count_freq;
    public static Integer MAX_SUFFIX_LENGTH = 5;
    public Double lambda1 = 0.31, lambda2 = 0.69;
    public static int error_cnt = 0;
    
    public HMM_Fast_Tagger()
    {
        states = new HashSet< String>();
        token_cnt = 0;
        tag_cnt = 0;
        bigram_cnt = 0;
        most_frequent_tag = "";
        unigram_freq = new HashMap< String, Integer>();
        bigram_freq = new HashMap< String, HashMap< String, Integer>>();
        emission_freq = new HashMap< String, HashMap< String, Integer>>();
        gt_unigram_freq = new HashMap< String, Double>();
        gt_bigram_freq = new HashMap< String, HashMap< String, Double>>();
        gt_emission_freq = new HashMap< String, HashMap< String, Double>>();
        bigram_with_count_freq = new HashMap< Integer, Integer>();
        emission_with_count_freq = new HashMap< Integer, Integer>();
    }

    public HMM_Fast_Tagger(Set<String> states, String most_frequent_tag, int token_cnt, int tag_cnt, int bigram_cnt, HashMap<String, Integer> unigram_freq, HashMap<String, HashMap<String, Integer>> bigram_freq, HashMap<String, HashMap<String, Integer>> emission_freq) 
    {
        this.states = states;
        this.most_frequent_tag = most_frequent_tag;
        this.token_cnt = token_cnt;
        this.tag_cnt = tag_cnt;
        this.bigram_cnt = bigram_cnt;
        this.unigram_freq = unigram_freq;
        this.bigram_freq = bigram_freq;
        this.emission_freq = emission_freq;
        gt_unigram_freq = new HashMap< String, Double>();
        gt_bigram_freq = new HashMap< String, HashMap< String, Double>>();
        gt_emission_freq = new HashMap< String, HashMap< String, Double>>();
        bigram_with_count_freq = new HashMap< Integer, Integer>();
        emission_with_count_freq = new HashMap< Integer, Integer>();
    }
    
    public ArrayList< String> best_tag_sequence( ArrayList< String> sentence)
    {
        // Data Structures
        HashMap< String, HashMap< Integer, Double>> seq_score = new HashMap< String, HashMap< Integer, Double>>();
        HashMap< String, HashMap< Integer, String>> back_ptr = new HashMap< String, HashMap< Integer, String>>();
        
        // Initialization
        Double max_score, cur_score;
        String candidate;
        int T = sentence.size();
        int N = states.size();
        for( String state: states)
        {
            seq_score.put( state, new HashMap< Integer, Double>());
            back_ptr.put( state, new HashMap< Integer, String>());
            for( Integer i = 0; i < T; i ++)
            {
                seq_score.get( state).put( i, 0.0);
                back_ptr.get( state).put( i, "");
            }
        }
        seq_score.get("SSS").put(0, 1.0);           // BAD CODE ALERT
        back_ptr.get("SSS").put(0, "");             // BAD CODE ALERT
        
        // Iteration
        for( int i = 1; i < T; i++)
        {
            String cur_word = sentence.get( i);
            for( String cur_state: states)
            {
                max_score = Double.MIN_VALUE;
                candidate = "";
                
                Double emit_score = get_emission_prob( cur_state, cur_word);
                if( emit_score == 0.0)
                    continue;

                for( String prev_state: states)
                {
                    if( prev_state.equals("."))
                        continue;
                    Double prev_score = seq_score.get( prev_state).get( i - 1);
                    
                    //Double trans_score = get_smooth_trans_prob( prev_state, cur_state);
                    Double trans_score = get_trans_prob( prev_state, cur_state);

                    cur_score = prev_score * trans_score * emit_score;
                    if( max_score < cur_score)
                    {
                        max_score = cur_score;
                        candidate = prev_state;
                    }
                }
                if( candidate.equals( ""))
                    candidate = most_frequent_tag;
                seq_score.get( cur_state).put(i, max_score);
                back_ptr.get( cur_state).put(i, candidate);
            }
        }
        
        ArrayList< String> C = new ArrayList< String>();
        for( int i = 0; i < T; i++)
            C.add( "");
        max_score = Double.MIN_VALUE;
        candidate = "";
        for( String state: states)
        {
            cur_score = seq_score.get( state).get( T - 1);
            if( max_score < cur_score)
            {
                max_score = cur_score;
                candidate = state;
            }
        }
        
        try
        {
            C.set( T - 1, candidate);
            for( int i = T - 2; i >= 0; i--)
                C.set( i, back_ptr.get( C.get(i + 1)).get(i + 1));                                    
        }
        catch( Exception e)
        {
            C.set( 0, "SSS");
            for( int i = 1; i < C.size() - 1; i++)
                C.set( i, most_frequent_tag);
            C.set( T - 1, ".");
            error_cnt ++;
        }
//        // something wicked
//        if( candidate.equals(""))
//        {
//            C.set( 0, "SSS");
//            for( int i = 1; i < C.size() - 1; i++)
//                C.set( i, most_frequent_tag);
//            C.set( T - 1, ".");
//        }
//        else
//        {
//            C.set( T - 1, candidate);
//            for( int i = T - 2; i >= 0; i--)
//                C.set( i, back_ptr.get( C.get(i + 1)).get(i + 1));            
//        }

        return C;
    }
    
    public void prepare_gt_counts()
    {
        for( String prev_tag: bigram_freq.keySet())
        {
            for( String cur_tag: bigram_freq.get( prev_tag).keySet())
            {
                int val = bigram_freq.get( prev_tag).get( cur_tag);
                if( bigram_with_count_freq.containsKey( val) == false)
                    bigram_with_count_freq.put( val, 0);
                Integer freq = bigram_with_count_freq.get( val);
                bigram_with_count_freq.put( val, freq + 1);
            }
        }
        for( String prev_tag: bigram_freq.keySet())
        {   
            gt_bigram_freq.put( prev_tag, new HashMap< String, Double>());
            Double prev_cnt = 0.0;
            for( String cur_tag: bigram_freq.get( prev_tag).keySet())
            {
                int C = bigram_freq.get( prev_tag).get( cur_tag);
                Double numA =  C + 1.0;
                Double numB = 0.0;
                try
                {
                    numB = bigram_with_count_freq.get( C + 1) * 1.0;
                }
                catch( Exception e)
                {
                    //System.out.println( "This should not happen ... Please do not happen");
                }
                Double denC = bigram_with_count_freq.get( C) * 1.0;
                Double C_star = ( numA * numB) / denC;
                gt_bigram_freq.get( prev_tag).put( cur_tag, C_star);
                prev_cnt += C_star;
            }
            gt_unigram_freq.put( prev_tag, prev_cnt);
        }
        
        for( String prev_tag: emission_freq.keySet())
        {
            for( String cur_tag: emission_freq.get( prev_tag).keySet())
            {
                int val = emission_freq.get( prev_tag).get( cur_tag);
                if( emission_with_count_freq.containsKey( val) == false)
                    emission_with_count_freq.put( val, 0);
                Integer freq = emission_with_count_freq.get( val);
                emission_with_count_freq.put( val, freq + 1);
            }
        }
        for( String prev_tag: emission_freq.keySet())
        {   
            gt_emission_freq.put( prev_tag, new HashMap< String, Double>());
            Double prev_cnt = 0.0;
            for( String cur_tag: emission_freq.get( prev_tag).keySet())
            {
                int C = emission_freq.get( prev_tag).get( cur_tag);
                Double numA =  C + 1.0;
                Double numB = 0.0;
                try
                {
                    numB = emission_with_count_freq.get( C + 1) * 1.0;
                }
                catch( Exception e)
                {
                    //System.out.println( "This should not happen ... Please do not happen");
                }
                Double denC = emission_with_count_freq.get( C) * 1.0;
                Double C_star = ( numA * numB) / denC;
                gt_emission_freq.get( prev_tag).put( cur_tag, C_star);
                prev_cnt += C_star;
            }
            //gt_unigram_freq.put( prev_tag, prev_cnt);
        }
    }
    
    public Double get_trans_prob( String prev_tag, String cur_tag)
    {
        if( IntC( unigram_freq, prev_tag) == 0)
            return 0.0;
        if( IntC( bigram_freq, prev_tag, cur_tag) == 0)
            return 0.0;
        return ( bigram_freq.get( prev_tag).get( cur_tag) * 1.0) / ( unigram_freq.get( prev_tag) * 1.0);
    }
    
    public Double get_gt_trans_prob( String prev_tag, String cur_tag)
    {
        Double val = DoubC( gt_bigram_freq, prev_tag, cur_tag);
        if( val > 0.0)
        {
            return val / DoubC( gt_unigram_freq, prev_tag);
        }
        Double num = bigram_with_count_freq.get( 1) * 1.0;
        Double den = bigram_cnt * 1.0;
        return num / den;
                
    }
    
    public Double get_smooth_trans_prob( String prev_tag, String cur_tag)
    {
        return ( lambda1 * DoubC( gt_unigram_freq, cur_tag)) + ( lambda2 * get_trans_prob( prev_tag, cur_tag));
    }

    public Double get_emission_prob( String tag, String word)
    {
        if( IntC( unigram_freq, tag) == 0)
            return 0.0;
        if( IntC( emission_freq, tag, word) == 0)
            return 0.0;
        return ( emission_freq.get( tag).get( word) * 1.0) / ( unigram_freq.get( tag) * 1.0);        
    }
    
    
    public Double get_gt_emission_prob( String tag, String word)
    {
        Double val = DoubC( gt_emission_freq, tag, tag);
        if( val > 0.0)
        {
            return val / DoubC( gt_unigram_freq, tag);
        }
        Double num = emission_with_count_freq.get( 1) * 1.0;
        Double den = bigram_cnt * 1.0;
        return num / den;
    }
    
    private Integer IntC( HashMap< String, Integer> map, String key)
    {
        if( map.containsKey( key))
            return map.get( key);
        return 0;
    }
    
    private Integer IntC( HashMap< String, HashMap< String, Integer>> map, String key1, String key2)
    {
        if( map.containsKey( key1))
            if( map.get( key1).containsKey( key2))
                return map.get( key1).get( key2);
        return 0;
    }
    
    private Double DoubC( HashMap< String, Double> map, String key)
    {
        if( map.containsKey( key))
            return map.get( key);
        return 0.0;
    }
    
    private Double DoubC( HashMap< String, HashMap< String, Double>> map, String key1, String key2)
    {
        if( map.containsKey( key1))
            if( map.get( key1).containsKey( key2))
                return map.get( key1).get( key2);
        return 0.0;
    }

}
